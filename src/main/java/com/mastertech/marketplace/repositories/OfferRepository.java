package com.mastertech.marketplace.repositories;

import org.springframework.data.repository.CrudRepository;

import com.mastertech.marketplace.models.Offer;
import com.mastertech.marketplace.models.Product;

public interface OfferRepository extends CrudRepository<Offer, Integer>{
	public Iterable<Offer> getAllByProduct(Product product);
}
